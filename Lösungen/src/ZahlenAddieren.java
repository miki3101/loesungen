// Erstellen Sie eine Variable in denen die Zahlen von 1 - 50 gespeichert sind
// Multipliziere alle Element und gib sie aus
// Erstellen Sie eine Variable in denen nur ungeraden Zahlen von 1 - 50 sind

public class ZahlenAddieren
{
	public static void main(String[] args)
	{
		int n = 50;
		int alle[] = new int[n];
		int ungerade[] = new int[n];
		double ergebnis = 1;

		// Erstellen sie eine Variable in denen die Zahlen von 1 - 50 gespeichert sind
		for (int i = 0; i < n; i++) // starte bei 0 und dann z�hle solange 1 dazu, bis du den wert n erreicht hast
		{
			alle[i] = i + 1; // Zahlen von 1 - 50 werden in das Array gespeichert

			System.out.println(alle[i]); // diese zahlen sind jetzt in lumpi gespeichert
		}

		
		// Multiplizieren Sie alle Zahlen in dieser Variable und geben Sie das Ergebnis aus
		for (int array : alle) // schau dir dir alle Zahlen in Lumpi an
		{
			ergebnis = ergebnis * array; // multipliziere jede Zahl in Lumpi
		}
		System.out.println(ergebnis); // Ergebnis dieser Multiplikation
		

		// Erstellen Sie eine Variable in denen nur die ungeraden Zahlen von 1 - 50 sind
		for (int i = 0; i < n; i++) //starte bei 0 und dann z�hle solange 1 dazu, bis du den Wert 50 erreicht hast
		{
			if ((i % 2) != 0) //wenn eine zahl in diesem Feld nicht durch 2 teilbar ist
			{
				ungerade[i] = i; //dann gib sie in das Feld ungerade

				System.out.println(ungerade[i]);
			}
		}
	}
}