// Definieren Sie ein Wort zB (Maus)
// geben Sie es von der Console verkehrt herum aus (suaM)


public class WortUmdrehen
{

	public static void main(String[] args)
	{
		String wort = new String("Maus"); // ich definiere ein neues Wort

		System.out.println(wort); // gibt mir das Wort aus

		int wortl�nge = wort.length(); // wortl�nge = die Anzahl der Zeichen des Wortes

		for (int i = 1; i < wortl�nge + 1; i++) // gehe jeden Buchstaben durch, bis zu am Ende bist
		{
			System.out.print(wort.charAt(wortl�nge - i)); // gibt mir die Buchstaben von der letzten bis zur ersten
															// Stelle aus
		}

	}

}
