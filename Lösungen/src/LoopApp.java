//Schleifen Beispiel

public class LoopApp
{
	// Schreiben Sie ein Programm mit dem die Reihe 1/2, 1/3, 1/4,... 1/100 summiert
	// wird und geben Sie das Ergebnis aus

	public static void main(String[] args)
	{
		double ergebnis = 0;
		int max = 100;

		for (double i = 2; i < max + 1; i++) // Z�hler f�ngt bei 2 an und z�hlt jede Runde 1 zum Nenner dazu
		{

			ergebnis = ergebnis + (1 / i); // Ergebnis ist Z�hler/Nenner, der sich jede Runde erh�ht

		}
		System.out.println(ergebnis); // gibt das Ergebnis der Summe der Br�che aus
		
		
		// Schreiben Sie in der selben Klasse ein Programm in dem die Reihe
		// 3+6+9+12+15...9000 summiert wird und geben Sie das Ergebnis aus

		int e = 0;									// ich starte bei 0
		int m = 9000;								// ich ende bei 9000
		
		for (int i = 3; i < m + 1; i = i + 3)		// ich fange bei 3 an, z�hle solange 3 dazu bis du 9000 hast
		{
			e = e + i;								// Ergebnis ist das derzeitige + die Zahl die ich um 3 erh�ht habe

		}
		System.out.println(e);						// gibt mir das Endergebnis aus
	}

}
